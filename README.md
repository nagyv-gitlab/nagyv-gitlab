# Hello! Szia!

I'm the Product Manager of the [Deploy stage at GitLab](https://handbook.gitlab.com/handbook/product/categories/#deploy-stage).

## Let's have a coffee chat ☕️

You can use [my Google Calendar Booking page](https://calendar.app.google/yppCdR1fC42axQR18) to easily initiate a coffee chat ☕️. I'm always happy to have a quick chat about GitLab and life 🛝

## What I work on 🔎

I recommend checking out [the categories](https://handbook.gitlab.com/handbook/product/categories/#deploy-stage) under the Deploy stage to learn more about my scope. To put it shortly: anything related to how you can use GitLab to deploy your own stuff or control the deployments belongs to me.

## Social Profiles 🌍

I'm actively using the following networks:

- [@nagyv-gitlab on GitLab.com](https://gitlab.com/nagyv-gitlab)
- [@nagyv on LinkedIn](https://www.linkedin.com/in/nagyv/)
